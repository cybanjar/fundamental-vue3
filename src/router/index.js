import { createWebHistory, createRouter } from "vue-router";

import Home from '../pages/Home.vue'
import Detail from '../pages/Detail.vue'
import User from '../pages/Users.vue'

const routes = [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/detail",
        name: "detail",
        component: Detail
    },
    {
        path: "/users",
        name: "users",
        component: User
    },
    {
        path: "/users/:userId",
        name: "users",
        component: User,
        props: true
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;